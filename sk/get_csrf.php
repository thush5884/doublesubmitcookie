<?php

include 'csrf_lib.php';

session_start();

$email = $_SESSION['user'];
$token = GenerateToken();
$cookie_name = "token";
setcookie($cookie_name, $token, time() + (86400 * 30), "/");
echo json_encode(array('token' => $token));


?>